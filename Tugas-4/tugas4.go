package main

import (
	"fmt"
)

func main() {
	// Soal 1
	fmt.Println("----Soal 1----")
	var kata string
	for i := 1; i <= 20; i++ {
		if i%3 == 0 && i%2 == 1 {
			kata = "I Love Coding"
		} else if i%2 == 0 {
			kata = "Berkualitas"
		} else {
			kata = "Santai"
		}
		fmt.Println(i, "-", kata)
	}

	// Soal 2
	fmt.Println("----Soal 2----")
	for i := 1; i <= 7; i++ {
		for j := 1; j <= i; j++ {
			fmt.Printf("#")
		}
		fmt.Printf("\n")
	}

	// Soal 3
	fmt.Println("----Soal 3----")
	var kalimat = [...]string{"aku", "dan", "saya", "sangat", "senang", "belajar", "golang"}
	fmt.Println(kalimat[2:7])

	// Soal 4
	fmt.Println(("----Soal 4----"))
	var sayuran = []string{"Bayam", "Buncis", "Kangkung", "Kubis", "Seledri", "Tauge", "Timun"}
	for nomor, sayur := range sayuran {
		fmt.Println(nomor+1, sayur)
	}

	// Soal 5
	fmt.Println("----Soal 5----")
	var satuan = map[string]int{
		"panjang": 7,
		"lebar":   4,
		"tinggi":  6,
	}
	satuan["volume balok"] = satuan["panjang"] * satuan["lebar"] * satuan["tinggi"]
	for key, value := range satuan {
		fmt.Println(key, "=", value)
	}
}
