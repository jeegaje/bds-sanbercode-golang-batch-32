package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

type NilaiMahasiswa struct {
	Nama        string `json:"nama"`
	MataKuliah  string `json:"mataKuliah"`
	IndeksNilai string `json:"indeksNilai"`
	Nilai       uint   `json:"nilai"`
	ID          uint   `json:"id"`
}

var nilaiNilaiMahasiswa = []NilaiMahasiswa{}

func getIndeks(nilai uint) string {
	if nilai >= 80 {
		return "A"
	} else if nilai >= 70 {
		return "B"
	} else if nilai >= 60 {
		return "C"
	} else if nilai >= 50 {
		return "D"
	} else {
		return "E"
	}
}

func Post(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		var Data NilaiMahasiswa
		if r.Header.Get("Content-Type") == "application/json" {
			// parse dari json
			decodeJSON := json.NewDecoder(r.Body)
			if err := decodeJSON.Decode(&Data); err != nil {
				log.Fatal(err)
			}
		} else {
			// parse dari form
			nama := r.PostFormValue("nama")
			mataKuliah := r.PostFormValue("mataKuliah")
			getNilai := r.PostFormValue("nilai")
			nilai, _ := strconv.Atoi(getNilai)
			Data = NilaiMahasiswa{
				Nama:       nama,
				MataKuliah: mataKuliah,
				Nilai:      uint(nilai),
			}
		}
		if Data.Nilai > 100 {
			http.Error(w, "Nilai tidak boleh lebih dari 100", http.StatusBadRequest)
		}
		Data.IndeksNilai = getIndeks(Data.Nilai)
		Data.ID = uint(len(nilaiNilaiMahasiswa) + 1)
		nilaiNilaiMahasiswa = append(nilaiNilaiMahasiswa, Data)
		dataNilaiMahasiswa, _ := json.Marshal(Data) // to byte
		w.Write(dataNilaiMahasiswa)                 // cetak di browser
		return
	}

	http.Error(w, "NOT FOUND", http.StatusNotFound)
	return
}

func Get(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		dataNilaiMahasiswa, err := json.Marshal(nilaiNilaiMahasiswa)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(dataNilaiMahasiswa)
		return
	}
	http.Error(w, "ERROR....", http.StatusNotFound)
}

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, pass, ok := r.BasicAuth()
		if !ok {
			w.Write([]byte("Username atau Password tidak boleh kosong"))
			return
		}

		if username == "admin123" && pass == "admin123" {
			next.ServeHTTP(w, r)
			return
		}
		w.Write([]byte("Username atau Password Salah"))
		return
	})
}

func main() {
	http.Handle("/nilai/create", Auth(http.HandlerFunc(Post)))
	http.HandleFunc("/nilai", Get)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}

}
