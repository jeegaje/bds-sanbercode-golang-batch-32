package main

import (
	"fmt"
	"math"
	"strconv"
	"sync"
	"time"
)

func showPhone(data []string, wg *sync.WaitGroup) {
	for number, item := range data {
		time.Sleep(time.Second)
		fmt.Printf("%d. %s\n", number+1, item)
	}
	wg.Done()
}

func getMovies(ch chan string, data ...string) {
	println("list movies:")
	for number, item := range data {
		ch <- strconv.Itoa(number+1) + ". " + item
	}
	close(ch)
}

func luasLigkaran(ch chan string, jari int) {
	var kalimat string = "luas lingkaran dengan jari jari " + strconv.Itoa(jari) + " :: " + strconv.Itoa(int(math.Round(math.Pi*float64(jari*jari))))
	ch <- kalimat

}

func kelilingLigkaran(ch chan string, jari int) {
	var kalimat string = "keliling lingkaran dengan jari jari " + strconv.Itoa(jari) + " :: " + strconv.Itoa(int(math.Round(2*math.Pi*float64(jari))))
	ch <- kalimat
}

func volumeTabung(ch chan string, jari int, tinggi int) {
	var kalimat string = "volume tabung dengan jari jari " + strconv.Itoa(jari) + " dan tinggi " + strconv.Itoa(tinggi) + " :: " + strconv.Itoa(int(math.Round(math.Pi*float64(jari*jari*tinggi))))
	ch <- kalimat
}

func luasPersegi(panjang int, lebar int, ch chan int) {
	ch <- panjang * lebar
}

func kelilingPersegi(panjang int, lebar int, ch chan int) {
	ch <- 2 * (panjang + lebar)
}

func volumeBalok(panjang int, lebar int, tinggi int, ch chan int) {
	ch <- panjang * lebar * tinggi
}

func main() {
	// Soal 1
	fmt.Println("----Soal 1----")
	var phones = []string{"Xiaomi", "Asus", "Iphone", "Samsung", "Oppo", "Realme", "Vivo"}
	var wg sync.WaitGroup
	wg.Add(1)
	go showPhone(phones, &wg)
	wg.Wait()

	// Soal 2
	fmt.Println("----Soal 2----")
	var movies = []string{"Harry Potter", "LOTR", "SpiderMan", "Logan", "Avengers", "Insidious", "Toy Story"}

	moviesChannel := make(chan string)

	go getMovies(moviesChannel, movies...)

	for value := range moviesChannel {
		fmt.Println(value)
	}

	// Soal 3
	fmt.Println("----Soal 3----")
	result := make(chan string)
	go luasLigkaran(result, 8)
	go luasLigkaran(result, 14)
	go luasLigkaran(result, 20)
	go kelilingLigkaran(result, 8)
	go kelilingLigkaran(result, 14)
	go kelilingLigkaran(result, 20)
	go volumeTabung(result, 8, 10)
	go volumeTabung(result, 14, 10)
	go volumeTabung(result, 20, 10)
	hasil1 := <-result
	fmt.Println(hasil1)
	hasil2 := <-result
	fmt.Println(hasil2)
	hasil3 := <-result
	fmt.Println(hasil3)
	hasil4 := <-result
	fmt.Println(hasil4)
	hasil5 := <-result
	fmt.Println(hasil5)
	hasil6 := <-result
	fmt.Println(hasil6)
	hasil7 := <-result
	fmt.Println(hasil7)
	hasil8 := <-result
	fmt.Println(hasil8)
	hasil9 := <-result
	fmt.Println(hasil9)

	// Soal 4
	fmt.Println("----Soal 4----")
	var ch1 = make(chan int)
	go luasPersegi(12, 4, ch1)

	var ch2 = make(chan int)
	go kelilingPersegi(12, 4, ch2)

	var ch3 = make(chan int)
	go volumeBalok(12, 4, 5, ch3)

	for i := 0; i < 3; i++ {
		select {
		case luas := <-ch1:
			fmt.Printf("Luas Persegi \t\t: %d \n", luas)
		case keliling := <-ch2:
			fmt.Printf("Keliling Persegi \t: %d \n", keliling)
		case volume := <-ch3:
			fmt.Printf("Volume Balok \t\t: %d \n", volume)
		}
	}
}
