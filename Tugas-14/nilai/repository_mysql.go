package nilai

import (
	"Tugas-14/config"
	"Tugas-14/models"
	"Tugas-14/utils"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
)

const (
	table          = "data_nilai"
	layoutDateTime = "2006-01-02 15:04:05"
)

// GetAll
func GetAll(ctx context.Context) ([]models.NilaiMahasiswa, error) {
	var dataNilai []models.NilaiMahasiswa
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v", table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var nilai models.NilaiMahasiswa
		//var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&nilai.ID,
			&nilai.Nama,
			&nilai.MataKuliah,
			&nilai.IndeksNilai,
			&nilai.Nilai); err != nil {
			return nil, err
		}
		/*
		   //  Change format string to datetime for created_at and updated_at
		   movie.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		   if err != nil {
		     log.Fatal(err)
		   }

		   movie.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		   if err != nil {
		     log.Fatal(err)
		   }
		*/
		dataNilai = append(dataNilai, nilai)
	}
	return dataNilai, nil
}

// Insert
func Insert(ctx context.Context, dataNilai models.NilaiMahasiswa) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	dataNilai.IndeksNilai = utils.GetIndeks(uint(dataNilai.Nilai))
	dataNilai.ID = uint(time.Now().Unix())
	queryText := fmt.Sprintf("INSERT INTO %v values(%v, '%v', '%v', '%v', %v)",
		table,
		dataNilai.ID,
		dataNilai.Nama,
		dataNilai.MataKuliah,
		dataNilai.IndeksNilai,
		dataNilai.Nilai)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

// Update
func Update(ctx context.Context, dataNilai models.NilaiMahasiswa, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	dataNilai.IndeksNilai = utils.GetIndeks(dataNilai.Nilai)
	queryText := fmt.Sprintf("UPDATE %v set nama ='%v', mataKuliah = '%v', indeksNilai = '%v', nilai = %v where id = %v",
		table,
		dataNilai.Nama,
		dataNilai.MataKuliah,
		dataNilai.IndeksNilai,
		dataNilai.Nilai,
		id,
	)

	_, err = db.ExecContext(ctx, queryText)
	if err != nil {
		return err
	}

	return nil
}

// Delete
func Delete(ctx context.Context, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = %v", table, id)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ditemukan")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}
