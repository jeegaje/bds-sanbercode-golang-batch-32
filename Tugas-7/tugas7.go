package main

import (
	"fmt"
)

type buah struct {
	nama, warna string
	adaBijinya  bool
	harga       int
}

type segitiga struct {
	alas, tinggi int
}

type persegi struct {
	sisi int
}

type persegiPanjang struct {
	panjang, lebar int
}

func (a segitiga) luasSegitiga() {
	luas := a.alas * a.tinggi / 2
	fmt.Println("Luas Segitiga ::", luas)
}
func (b persegi) luasPersegi() {
	luas := b.sisi * b.sisi
	fmt.Println("Luas Persegi ::", luas)
}
func (c persegiPanjang) luasPersegiPanjang() {
	luas := c.panjang * c.lebar
	fmt.Println("Luas Persegi Panjang ::", luas)
}

type phone struct {
	name, brand string
	year        int
	colors      []string
}

func (d *phone) tambahColor(warna ...string) {
	for _, item := range warna {
		d.colors = append(d.colors, item)
	}
}

type movie struct {
	title, genre   string
	duration, year int
}

func tambahDataFilm(nama string, durasi int, genre string, tahun int, dataFilm *[]movie) {
	var data = movie{title: nama, genre: genre, duration: durasi, year: tahun}
	*dataFilm = append(*dataFilm, data)
}

func main() {
	// Soal 1
	fmt.Println("----Soal 1----")
	var nanas = buah{"Nanas", "Kuning", false, 9000}
	var jeruk = buah{"Jeruk", "Oranye", true, 8000}
	var semangka = buah{"Semangka", "Hijau & Merah", true, 10000}
	var pisang = buah{"Pisang", "Kuning", false, 5000}

	fmt.Println(nanas)
	fmt.Println(jeruk)
	fmt.Println(semangka)
	fmt.Println(pisang)

	// Soal 2
	fmt.Println("----Soal 2----")
	var contohSegitiga = segitiga{6, 10}
	contohSegitiga.luasSegitiga()
	var contohPersegi = persegi{10}
	contohPersegi.luasPersegi()
	var contohPersegiPanjang = persegiPanjang{8, 12}
	contohPersegiPanjang.luasPersegiPanjang()

	// Soal 3
	fmt.Println("----Soal 3----")
	var exTambahwarna = phone{name: "galaxy", brand: "samsung", year: 2000}
	exTambahwarna.tambahColor("hijau", "coklat")
	fmt.Println(exTambahwarna)

	// Soal 4
	fmt.Println("----Soal 4----")
	var dataFilm = []movie{}

	tambahDataFilm("LOTR", 120, "action", 1999, &dataFilm)
	tambahDataFilm("avenger", 120, "action", 2019, &dataFilm)
	tambahDataFilm("spiderman", 120, "action", 2004, &dataFilm)
	tambahDataFilm("juon", 120, "horror", 2004, &dataFilm)

	// isi dengan jawaban anda untuk menampilkan data
	for number, item := range dataFilm {
		fmt.Printf("%d. title : %s\n   duration : %d\n   genre : %s\n   year : %d\n\n", number+1, item.title, item.duration, item.genre, item.year)
	}
}
