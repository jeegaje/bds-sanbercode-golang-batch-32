// Bootcamp Digital Skill Sanbercode Golang
package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// Soal 1
	var teks1 = "Bootcamp"
	var teks2 = "Digital"
	var teks3 = "Skill"
	var teks4 = "Sanbercode"
	var teks5 = "Golang"
	var teksGabung = teks1 + " " + teks2 + " " + teks3 + " " + teks4 + " " + teks5
	fmt.Println(teksGabung)

	// Soal 2
	halo := "Halo Dunia"
	find := "Dunia"
	replace := "Golang"
	teksBaru := strings.Replace(halo, find, replace, 1)
	fmt.Println(teksBaru)

	// Soal 3
	var kataPertama = "saya"
	var kataKedua = "senang"
	var kataKetiga = "belajar"
	var kataKeempat = "golang"
	var upperFirst = strings.ToUpper(string(kataKedua[0]))
	var upperLast = strings.ToUpper(string(kataKetiga[6]))
	var kataKeduaBaru = strings.Replace(kataKedua, "s", upperFirst, 1)
	var kataKetigaBaru = strings.Replace(kataKetiga, "r", upperLast, 1)
	var kataKeempatBaru = strings.ToUpper(kataKeempat)
	var kataBaru = kataPertama + " " + kataKeduaBaru + " " + kataKetigaBaru + " " + kataKeempatBaru
	fmt.Println(kataBaru)

	// Soal 4
	var angkaPertama = "8"
	var angkaKedua = "5"
	var angkaKetiga = "6"
	var angkaKeempat = "7"
	var angkaPertamaInt, err1 = strconv.Atoi(angkaPertama)
	var angkaKeduaInt, err2 = strconv.Atoi(angkaKedua)
	var angkaketigaInt, err3 = strconv.Atoi(angkaKetiga)
	var angkaKeempatInt, err4 = strconv.Atoi(angkaKeempat)

	if err1 == nil && err2 == nil && err3 == nil && err4 == nil {
		var jumlah = angkaPertamaInt + angkaKeduaInt + angkaketigaInt + angkaKeempatInt
		fmt.Println(jumlah)
	}

	// Soal 5
	kalimat := "halo halo bandung"
	angka := 2021
	kalimatBaru := strings.Replace(kalimat, "halo", "HI", 2)
	angkaBaru := strconv.Itoa(angka)
	fmt.Println(`"` + kalimatBaru + `" - ` + angkaBaru)
}
