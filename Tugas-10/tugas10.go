package main

import (
	"errors"
	"flag"
	"fmt"
	"math"
	"strconv"
	"time"
)

func data(kalimat string, tahun int) {
	fmt.Println(kalimat, strconv.Itoa(tahun))
}

func kelilingSegitigaSamaSisi(sisi int, kondisi bool) (string, error) {
	switch {
	case sisi > 0 && kondisi:
		return "keliling segitiga sama sisinya dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(sisi+sisi+sisi) + " cm", nil
	case sisi > 0 && !kondisi:
		return strconv.Itoa(sisi), nil
	case sisi == 0 && kondisi:
		return "", errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi")
	default:
		message := recover()
		fmt.Println("panic:", message)
		return "", errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi")
	}
}

func tambahAngka(ankga1 int, angka2 *int) {
	*angka2 += ankga1
}

func cetakAngka(angka *int) {
	fmt.Println("Hasil cetak angka ::", *angka)
}

func addPhone(database *[]string, phone ...string) {
	for _, item := range phone {
		*database = append(*database, item)
	}
}

func luasLigkaran(jari int) float64 {
	return math.Floor(math.Pi * float64(jari*jari))
}

func kelilingLigkaran(jari int) float64 {
	return math.Floor(2 * math.Pi * float64(jari))
}

func main() {
	// deklarasi variabel angka ini simpan di baris pertama func main
	angka := 1
	// Soal 1
	defer data("Golang Backend Development", 2021)

	// Soal 2

	fmt.Println(kelilingSegitigaSamaSisi(4, true))

	fmt.Println(kelilingSegitigaSamaSisi(8, false))

	fmt.Println(kelilingSegitigaSamaSisi(0, true))

	fmt.Println(kelilingSegitigaSamaSisi(0, false))

	// Soal 3

	defer cetakAngka(&angka)

	tambahAngka(7, &angka)

	tambahAngka(6, &angka)

	tambahAngka(-1, &angka)

	tambahAngka(9, &angka)

	// Soal 4

	var phones = []string{}
	addPhone(&phones, "Xiaomi", "Asus", "IPhone", "Samsung", "Oppo", "Realme", "Vivo")
	for number, item := range phones {
		time.Sleep(time.Second)
		fmt.Println(number+1, item)
	}

	// Soal 5
	fmt.Println("Luas lingkarang ::", luasLigkaran(7))
	fmt.Println("Luas lingkarang ::", luasLigkaran(10))
	fmt.Println("Luas lingkarang ::", luasLigkaran(15))
	fmt.Println("Keliling lingkarang ::", kelilingLigkaran(7))
	fmt.Println("Keliling lingkarang ::", kelilingLigkaran(10))
	fmt.Println("Keliling lingkarang ::", kelilingLigkaran(15))

	// Soal 6
	var panjang = flag.Int("panjang", 30, "masukkan panjang")
	var lebar = flag.Int("lebar", 20, "masukkan lebar")

	flag.Parse()
	fmt.Printf("Luas persegi panjang\t:: %d\n", *panjang**lebar)
	fmt.Printf("Keliling persegi panjang\t:: %d\n", 2*(*panjang+*lebar))
}
