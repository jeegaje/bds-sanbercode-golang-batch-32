package book

import (
	"Quiz-3/config"
	"Quiz-3/models"
	"Quiz-3/utils"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net/url"
	"time"
)

const (
	table          = "book"
	layoutDateTime = "2006-01-02 15:04:05"
)

// GetAll
func GetAll(ctx context.Context) ([]models.Book, error) {
	var dataBook []models.Book
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v", table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var book models.Book
		var created_at, updated_at string
		//var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&book.Id,
			&book.Title,
			&book.Description,
			&book.Image_url,
			&book.Release_year,
			&book.Price,
			&book.Total_page,
			&book.Thickness,
			&created_at,
			&updated_at,
			&book.Category_id); err != nil {
			return nil, err
		}
		book.Created_at, err = time.Parse(layoutDateTime, created_at)
		if err != nil {
			log.Fatal(err)
		}
		book.Updated_at, err = time.Parse(layoutDateTime, updated_at)
		if err != nil {
			log.Fatal(err)
		}

		dataBook = append(dataBook, book)
	}
	return dataBook, nil
}

// Insert
func Insert(ctx context.Context, dataBook models.Book) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	if _, err := url.ParseRequestURI(dataBook.Image_url); err != nil && (dataBook.Release_year < 1980 || dataBook.Release_year > 2021) {
		return errors.New("release_year tidak boleh melebih 2021 dan kurang dari 1980 dan image_url harus diisi dengan url")
	} else if _, err := url.ParseRequestURI(dataBook.Image_url); err != nil {
		return errors.New("image_url harus diisi dengan url")
	} else if dataBook.Release_year < 1980 || dataBook.Release_year > 2021 {
		return errors.New("release_year tidak boleh melebih 2021 dan kurang dari 1980")
	}

	queryText := fmt.Sprintf("INSERT INTO %v (title, description, image_url, release_year, price, total_page, thickness, created_at, updated_at, category_id) values('%v', '%v', '%v', %v, '%v', %v, '%v', NOW(), NOW(), %v)",
		table,
		dataBook.Title,
		dataBook.Description,
		dataBook.Image_url,
		dataBook.Release_year,
		dataBook.Price,
		dataBook.Total_page,
		utils.GetThickness(dataBook.Total_page),
		dataBook.Category_id,
	)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func Update(ctx context.Context, dataBook models.Book, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	if _, err := url.ParseRequestURI(dataBook.Image_url); err != nil && (dataBook.Release_year < 1980 || dataBook.Release_year > 2021) {
		return errors.New("release_year tidak boleh melebih 2021 dan kurang dari 1980 dan image_url harus diisi dengan url")
	} else if _, err := url.ParseRequestURI(dataBook.Image_url); err != nil {
		return errors.New("image_url harus diisi dengan url")
	} else if dataBook.Release_year < 1980 || dataBook.Release_year > 2021 {
		return errors.New("release_year tidak boleh melebih 2021 dan kurang dari 1980")
	}

	queryText := fmt.Sprintf("UPDATE %v set title ='%v', description = '%v', image_url = '%v', release_year = %v, price = '%v', total_page = %v, thickness = '%v', updated_at = NOW() where id = %v",
		table,
		dataBook.Title,
		dataBook.Description,
		dataBook.Image_url,
		dataBook.Release_year,
		dataBook.Price,
		dataBook.Total_page,
		utils.GetThickness(dataBook.Total_page),
		id,
	)

	_, err = db.ExecContext(ctx, queryText)
	if err != nil {
		return err
	}

	return nil
}

// Delete
func Delete(ctx context.Context, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = %v", table, id)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ditemukan")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}

// filter
func filterTitle(ctx context.Context, title string) ([]models.Book, error) {
	var dataBook []models.Book
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v", table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}
	for rowQuery.Next() {
		var book models.Book
		var created_at, updated_at string
		//var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&book.Id,
			&book.Title,
			&book.Description,
			&book.Image_url,
			&book.Release_year,
			&book.Price,
			&book.Total_page,
			&book.Thickness,
			&created_at,
			&updated_at,
			&book.Category_id); err != nil {
			return nil, err
		}
		book.Created_at, err = time.Parse(layoutDateTime, created_at)
		if err != nil {
			log.Fatal(err)
		}
		book.Updated_at, err = time.Parse(layoutDateTime, updated_at)
		if err != nil {
			log.Fatal(err)
		}

		dataBook = append(dataBook, book)
	}
	return dataBook, nil
}
