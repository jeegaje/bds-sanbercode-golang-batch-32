package category

import (
	"Quiz-3/config"
	"Quiz-3/models"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
)

const (
	table          = "category"
	layoutDateTime = "2006-01-02 15:04:05"
)

// GetAll
func GetAll(ctx context.Context) ([]models.Category, error) {
	var dataCategory []models.Category
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v", table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var category models.Category
		var created_at, updated_at string
		//var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&category.Id,
			&category.Name,
			&created_at,
			&updated_at); err != nil {
			return nil, err
		}
		category.Created_at, err = time.Parse(layoutDateTime, created_at)
		if err != nil {
			log.Fatal(err)
		}
		category.Updated_at, err = time.Parse(layoutDateTime, updated_at)
		if err != nil {
			log.Fatal(err)
		}

		dataCategory = append(dataCategory, category)
	}
	return dataCategory, nil
}

// Insert
func Insert(ctx context.Context, dataCategory models.Category) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	queryText := fmt.Sprintf("INSERT INTO %v (name, created_at, updated_at) values('%v', NOW(), NOW())",
		table,
		dataCategory.Name)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func Update(ctx context.Context, dataCategory models.Category, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	queryText := fmt.Sprintf("UPDATE %v set name ='%v', updated_at = NOW() where id = %v",
		table,
		dataCategory.Name,
		id,
	)

	_, err = db.ExecContext(ctx, queryText)
	if err != nil {
		return err
	}

	return nil
}

// Delete
func Delete(ctx context.Context, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = %v", table, id)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ditemukan")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}
