package main

import (
	"Quiz-3/book"
	"Quiz-3/category"
	"Quiz-3/models"
	"Quiz-3/utils"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

func main() {
	router := httprouter.New()
	// Route Bangun Datar
	router.GET("/bangun-datar/segitiga-sama-sisi", GetSegitigaSamaSisi)
	router.GET("/bangun-datar/persegi", GetPersegi)
	router.GET("/bangun-datar/persegi-panjang", GetPersegiPanjang)
	router.GET("/bangun-datar/lingkaran", GetLingkaran)
	router.GET("/bangun-datar/jajar-genjang", GetJajarGenjang)

	// Route Category
	router.GET("/categories", GetAllCtgr)
	router.POST("/categories", utils.BasicAuth(PostCategory))
	router.PUT("/categories/:id", utils.BasicAuth(UpdateCategory))
	router.DELETE("/categories/:id", utils.BasicAuth(DeleteCategory))

	// Route Book
	router.GET("/books", GetBook)
	router.POST("/books", utils.BasicAuth(PostBook))
	router.PUT("/books/:id", utils.BasicAuth(UpdateBook))
	router.DELETE("/books/:id", utils.BasicAuth(DeleteBook))

	fmt.Println("Server Running at Port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))

}

func GetSegitigaSamaSisi(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		var Data models.SegitigaSamaSisi
		// parse dari form
		getAlas := r.URL.Query().Get("alas")
		getTinggi := r.URL.Query().Get("tinggi")
		getHitung := r.URL.Query().Get("hitung")
		alas, _ := strconv.Atoi(getAlas)
		tinggi, _ := strconv.Atoi(getTinggi)
		Data = models.SegitigaSamaSisi{
			Alas:   uint(alas),
			Tinggi: uint(tinggi),
		}
		if getHitung == "luas" {
			Data.Luas = utils.LuasSegitigaSamaSisi(Data.Alas, Data.Tinggi)
		} else if getHitung == "keliling" {
			Data.Keliling = utils.KelilingSegitigaSamaSisi(Data.Alas)
		}
		utils.ResponseJSON(w, Data, http.StatusCreated)
	}
}

func GetPersegi(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		var Data models.Persegi
		// parse dari form
		getSisi := r.URL.Query().Get("sisi")
		getHitung := r.URL.Query().Get("hitung")
		sisi, _ := strconv.Atoi(getSisi)
		Data = models.Persegi{
			Sisi: uint(sisi),
		}
		if getHitung == "luas" {
			Data.Luas = utils.LuasPersegi(Data.Sisi)
		} else if getHitung == "keliling" {
			Data.Keliling = utils.KelilingPersegi(Data.Sisi)
		}
		utils.ResponseJSON(w, Data, http.StatusCreated)
	}
}

func GetPersegiPanjang(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		var Data models.PersegiPanjang
		// parse dari form
		getPanjang := r.URL.Query().Get("panjang")
		getLebar := r.URL.Query().Get("lebar")
		getHitung := r.URL.Query().Get("hitung")
		panjang, _ := strconv.Atoi(getPanjang)
		lebar, _ := strconv.Atoi(getLebar)
		Data = models.PersegiPanjang{
			Panjang: uint(panjang),
			Lebar:   uint(lebar),
		}
		if getHitung == "luas" {
			Data.Luas = utils.LuasPersegiPanjang(Data.Panjang, Data.Lebar)
		} else if getHitung == "keliling" {
			Data.Keliling = utils.KelilingPersegiPanjang(Data.Panjang, Data.Lebar)
		}
		utils.ResponseJSON(w, Data, http.StatusCreated)
	}
}

func GetLingkaran(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		var Data models.Lingkaran
		// parse dari form
		getJariJari := r.URL.Query().Get("jariJari")
		getHitung := r.URL.Query().Get("hitung")
		jarijari, _ := strconv.Atoi(getJariJari)
		Data = models.Lingkaran{
			JariJari: uint(jarijari),
		}
		if getHitung == "luas" {
			Data.Luas = utils.LuasLigkaran(Data.JariJari)
		} else if getHitung == "keliling" {
			Data.Keliling = utils.KelilingLigkaran(Data.JariJari)
		}
		utils.ResponseJSON(w, Data, http.StatusCreated)
	}
}

func GetJajarGenjang(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		var Data models.JajarGenjang
		// parse dari form
		getSisi := r.URL.Query().Get("sisi")
		getAlas := r.URL.Query().Get("alas")
		getTinggi := r.URL.Query().Get("tinggi")
		getHitung := r.URL.Query().Get("hitung")

		sisi, _ := strconv.Atoi(getSisi)
		alas, _ := strconv.Atoi(getAlas)
		tinggi, _ := strconv.Atoi(getTinggi)
		Data = models.JajarGenjang{
			Sisi:   uint(sisi),
			Alas:   uint(alas),
			Tinggi: uint(tinggi),
		}
		if getHitung == "luas" {
			Data.Luas = utils.LuasJajarGenjang(Data.Alas, Data.Tinggi)
		} else if getHitung == "keliling" {
			Data.Keliling = utils.KelilingJajarGenjang(Data.Alas, Data.Tinggi, Data.Sisi)
		}
		utils.ResponseJSON(w, Data, http.StatusCreated)
	}
}

// CATEGORY
// GEt
func GetAllCtgr(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	data, err := category.GetAll(ctx)

	if err != nil {
		fmt.Println(err)
	}

	utils.ResponseJSON(w, data, http.StatusOK)
}

// Post
func PostCategory(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var data models.Category
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}
	if err := category.Insert(ctx, data); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusCreated)
}

// Update
func UpdateCategory(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var data models.Category

	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	var id = ps.ByName("id")

	if err := category.Update(ctx, data, id); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

// Delete
func DeleteCategory(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var id = ps.ByName("id")
	if err := category.Delete(ctx, id); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusOK)
}

// BOOK
// GEt
func GetBook(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	data, err := book.GetAll(ctx)

	if err != nil {
		fmt.Println(err)
	}

	utils.ResponseJSON(w, data, http.StatusOK)
}

// Post
func PostBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var data models.Book
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}
	if err := book.Insert(ctx, data); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusCreated)
}

// Update
func UpdateBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var data models.Book

	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	var id = ps.ByName("id")

	if err := book.Update(ctx, data, id); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

// Delete
func DeleteBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var id = ps.ByName("id")
	if err := book.Delete(ctx, id); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusOK)
}

func filterBook(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	//getTitle := r.URL.Query().Get("title")
	data, err := book.GetAll(ctx)

	if err != nil {
		fmt.Println(err)
	}

	utils.ResponseJSON(w, data, http.StatusOK)
}
