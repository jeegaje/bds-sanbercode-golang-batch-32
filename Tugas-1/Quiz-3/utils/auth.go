package utils

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func BasicAuth(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		// Get the Basic Authentication credentials
		username, password, hasAuth := r.BasicAuth()
		if !hasAuth {
			w.Write([]byte("Username atau Password tidak boleh kosong"))
			return
		}
		if hasAuth && username == "admin" && password == "password" {
			// Delegate request to the given handle
			h(w, r, ps)
		} else if hasAuth && username == "editor" && password == "secret" {
			// Delegate request to the given handle
			h(w, r, ps)
		} else if hasAuth && username == "trainer" && password == "rahasia" {
			// Delegate request to the given handle
			h(w, r, ps)
		} else {
			w.Write([]byte("Username atau Password Salah"))
		}
	}
}
