package utils

import (
	"encoding/json"
	"math"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

func ResponseJSON(w http.ResponseWriter, p interface{}, status int) {
	ubahkeByte, err := json.Marshal(p)
	w.Header().Set("Content-Type", "application/json")

	if err != nil {
		http.Error(w, "error om", http.StatusBadRequest)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(ubahkeByte))
}

func LuasSegitigaSamaSisi(alas uint, tinggi uint) uint {
	luas := uint(alas * tinggi / 2)
	return luas
}

func LuasPersegi(sisi uint) uint {
	luas := uint(sisi * sisi)
	return luas
}

func LuasPersegiPanjang(panjang uint, lebar uint) uint {
	luas := uint(panjang * lebar)
	return luas
}

func LuasLigkaran(jari uint) uint {
	luas := uint(math.Round(math.Pi * float64(jari*jari)))
	return luas
}

func LuasJajarGenjang(alas uint, tinggi uint) uint {
	luas := uint(alas * tinggi)
	return luas
}

func KelilingSegitigaSamaSisi(alas uint) uint {
	keliling := uint(alas + alas + alas)
	return keliling
}

func KelilingPersegi(sisi uint) uint {
	keliling := uint(2 * (sisi + sisi))
	return keliling
}

func KelilingPersegiPanjang(panjang uint, lebar uint) uint {
	keliling := uint(2 * (panjang + lebar))
	return keliling
}

func KelilingLigkaran(jari uint) uint {
	keliling := uint(math.Round(2 * math.Pi * float64(jari)))
	return keliling
}

func KelilingJajarGenjang(alas uint, tinggi uint, sisi uint) uint {
	keliling := uint(2*alas + 2*sisi)
	return keliling
}

func GetThickness(page int) string {
	if page >= 201 {
		return "tebal"
	} else if page >= 101 {
		return "sedang"
	} else {
		return "tipis"
	}
}
