package models

type SegitigaSamaSisi struct {
	Alas     uint `json:"alas"`
	Tinggi   uint `json:"tinggi"`
	Luas     uint `json:"luas"`
	Keliling uint `json:"keliling"`
}

type Persegi struct {
	Sisi     uint `json:"sisi"`
	Luas     uint `json:"luas"`
	Keliling uint `json:"keliling"`
}

type PersegiPanjang struct {
	Panjang  uint `json:"panjang"`
	Lebar    uint `json:"lebar"`
	Luas     uint `json:"luas"`
	Keliling uint `json:"keliling"`
}

type Lingkaran struct {
	JariJari uint `json:"jariJari"`
	Luas     uint `json:"luas"`
	Keliling uint `json:"keliling"`
}

type JajarGenjang struct {
	Sisi     uint `json:"sisi"`
	Alas     uint `json:"alas"`
	Tinggi   uint `json:"tinggi"`
	Luas     uint `json:"luas"`
	Keliling uint `json:"keliling"`
}
