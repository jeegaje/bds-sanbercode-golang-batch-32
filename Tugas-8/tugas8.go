package main

import (
	"fmt"
	"math"
	"strconv"
)

type segitigaSamaSisi struct {
	alas, tinggi int
}

type persegiPanjang struct {
	panjang, lebar int
}

type tabung struct {
	jariJari, tinggi float64
}

type balok struct {
	panjang, lebar, tinggi int
}

type hitungBangunDatar interface {
	luas() int
	keliling() int
}

type hitungBangunRuang interface {
	volume() float64
	luasPermukaan() float64
}

func (s segitigaSamaSisi) luas() int {
	return s.alas * s.tinggi / 2
}

func (s segitigaSamaSisi) keliling() int {
	alas := s.alas / 2
	sisi := int(math.Sqrt(float64(alas*alas + s.tinggi*s.tinggi)))
	return sisi + sisi + sisi
}

func (p persegiPanjang) luas() int {
	return p.lebar * p.panjang
}

func (p persegiPanjang) keliling() int {
	return (p.lebar + p.panjang) * 2
}

func (t tabung) volume() float64 {
	return math.Pi * t.jariJari * t.jariJari * t.tinggi
}

func (t tabung) luasPermukaan() float64 {
	alas := 2 * math.Pi * t.jariJari * t.jariJari
	selimut := 2 * math.Pi * t.jariJari
	return alas + selimut
}

func (b balok) volume() float64 {
	return float64(b.lebar * b.panjang * b.tinggi)
}

func (b balok) luasPermukaan() float64 {
	return float64(2*b.panjang*b.lebar + 2*b.lebar*b.tinggi + 2*b.panjang*b.tinggi)
}

type phone struct {
	name, brand string
	year        int
	colors      []string
}

type gadget interface {
	showData() string
}

func (p *phone) tambahColor(warna ...string) {
	for _, item := range warna {
		p.colors = append(p.colors, item)
	}
}

func (p phone) showData() string {
	str := strconv.Itoa(p.year)
	kalimat := "name : " + p.name + "\nbrand : " + p.brand + "\nyear : " + str + "\ncolors : "
	for _, item := range p.colors {
		kalimat += item + ", "
	}
	return kalimat
}

func luasPersegi(sisi uint, keterangan bool) interface{} {
	switch {
	case sisi > 0 && keterangan:
		return "luas persegi dengan sisi " + strconv.Itoa(int(sisi)) + " cm adalah " + strconv.Itoa(int(sisi*sisi)) + " cm"
	case sisi > 0 && !keterangan:
		return sisi * sisi
	case sisi == 0 && keterangan:
		return "maaf anda belum menginput sisi persegi!"
	default:
		return nil
	}
}

func main() {
	// Soal 1
	fmt.Println("----Soal 1----")
	var bangunDatar hitungBangunDatar
	var bangunRuang hitungBangunRuang
	bangunDatar = segitigaSamaSisi{8, 3}
	fmt.Println("===== Segitiga sama sisi")
	fmt.Println("luas      :", bangunDatar.luas())
	fmt.Println("keliling  :", bangunDatar.keliling())
	bangunDatar = persegiPanjang{4, 8}
	fmt.Println("===== Persegi Panjang")
	fmt.Println("luas      :", bangunDatar.luas())
	fmt.Println("keliling  :", bangunDatar.keliling())
	bangunRuang = tabung{7, 5}
	fmt.Println("===== Tabung")
	fmt.Println("volume      :", bangunRuang.volume())
	fmt.Println("luas permukaan  :", bangunRuang.luasPermukaan())
	bangunRuang = balok{4, 6, 8}
	fmt.Println("===== Balok")
	fmt.Println("volume      :", bangunRuang.volume())
	fmt.Println("luas permukaan  :", bangunRuang.luasPermukaan())

	// Soal 2
	fmt.Println("----Soal 2----")
	var hape gadget
	hape = phone{
		name:   "Galaxy Note 20",
		brand:  "Samsung",
		year:   2020,
		colors: []string{"Mysthic Brown", "Mysthic White", "Mysthic Black"},
	}
	fmt.Println(hape.showData())

	// Soal 3
	fmt.Println("----Soal 3----")
	fmt.Println(luasPersegi(4, true))

	fmt.Println(luasPersegi(8, false))

	fmt.Println(luasPersegi(0, true))

	fmt.Println(luasPersegi(0, false))

	// Soal 4
	fmt.Println("----Soal 4----")
	var prefix interface{} = "hasil penjumlahan dari "

	var kumpulanAngkaPertama interface{} = []int{6, 8}

	var kumpulanAngkaKedua interface{} = []int{12, 14}

	// tulis jawaban anda disini

	var jumlah int

	kumpulanAngka := kumpulanAngkaPertama.([]int)

	kalimat := prefix.(string)

	angkaKedua := kumpulanAngkaKedua.([]int)
	kumpulanAngka = append(kumpulanAngka, angkaKedua...)

	for index, item := range kumpulanAngka {
		jumlah += item
		if index == len(kumpulanAngka)-1 {
			kalimat += strconv.Itoa(item) + "+" + strconv.Itoa(jumlah)

		} else {
			kalimat += strconv.Itoa(item) + "+"
		}
	}

	fmt.Println(kalimat)
}
