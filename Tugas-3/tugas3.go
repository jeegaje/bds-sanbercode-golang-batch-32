package main

import (
	"fmt"
	"strconv"
)

func main() {
	// Soal 1
	fmt.Println("----Soal 1----")
	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"
	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"

	intPanjangPersegiPanjang, _ := strconv.Atoi(panjangPersegiPanjang)
	intLebarPersegiPanjang, _ := strconv.Atoi(lebarPersegiPanjang)
	intAlasSegitiga, _ := strconv.Atoi(alasSegitiga)
	intTinggiSegitiga, _ := strconv.Atoi(tinggiSegitiga)

	var luasPersegiPanjang int = intPanjangPersegiPanjang * intLebarPersegiPanjang
	var kelilingPersegiPanjang int = 2 * (intPanjangPersegiPanjang + intLebarPersegiPanjang)
	var luasSegitiga int = intAlasSegitiga * intTinggiSegitiga / 2

	fmt.Printf("Luas Persegi Panjang :: %d\nKeliling Persegi Panjang :: %d\nLuas Segitia :: %d\n", luasPersegiPanjang, kelilingPersegiPanjang, luasSegitiga)

	// Soal 2
	fmt.Println("----Soal 2----")
	var nilaiJohn = 80
	var nilaiDoe = 50

	if nilaiJohn >= 80 {
		fmt.Println("Indeks Nilai John :: A")
	} else if nilaiJohn >= 70 {
		fmt.Println("Indeks Nilai John :: B")
	} else if nilaiJohn >= 60 {
		fmt.Println("Indeks Nilai John :: C")
	} else if nilaiJohn >= 50 {
		fmt.Println("Indeks Nilai John :: D")
	} else {
		fmt.Println("Indeks Nilai John :: E")
	}

	if nilaiDoe >= 80 {
		fmt.Println("Indeks Nilai Doe :: A")
	} else if nilaiDoe >= 70 {
		fmt.Println("Indeks Nilai Doe :: B")
	} else if nilaiDoe >= 60 {
		fmt.Println("Indeks Nilai Doe :: C")
	} else if nilaiDoe >= 50 {
		fmt.Println("Indeks Nilai Doe :: D")
	} else {
		fmt.Println("Indeks Nilai Doe :: E")
	}

	// Soal 3
	fmt.Println("----Soal 3----")
	var tanggal = 14
	var bulan = 8
	var tahun = 2001
	var namaBulan string

	switch bulan {
	case 1:
		namaBulan = "Januari"
	case 2:
		namaBulan = "Februari"
	case 3:
		namaBulan = "Maret"
	case 4:
		namaBulan = "April"
	case 5:
		namaBulan = "Mei"
	case 6:
		namaBulan = "Juni"
	case 7:
		namaBulan = "Juli"
	case 8:
		namaBulan = "Agustus"
	case 9:
		namaBulan = "September"
	case 10:
		namaBulan = "Oktober"
	case 11:
		namaBulan = "November"
	case 12:
		namaBulan = "Desember"
	}
	fmt.Println(tanggal, namaBulan, tahun)

	// Soal 4
	fmt.Println("----Soal 4----")
	var generasi string

	if tahun >= 1944 && tahun <= 1964 {
		generasi = "Baby Boomer"
	} else if tahun >= 1965 && tahun <= 1979 {
		generasi = "Generasi X"
	} else if tahun >= 1980 && tahun <= 1994 {
		generasi = "Generasi Y (Millenials)"
	} else if tahun >= 1995 && tahun <= 2015 {
		generasi = "Generasi Z"
	}
	fmt.Println("Istilah generasi tahun kelahiran saya ::", generasi)
}
