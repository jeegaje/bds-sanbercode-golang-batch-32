package nilai

import (
	"Tugas-15/config"
	"Tugas-15/models"
	"Tugas-15/utils"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
)

const (
	table = "nilai"
)

// GetAll
func GetAll(ctx context.Context) ([]models.Nilai, error) {
	var dataNilai []models.Nilai
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v", table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var nilai models.Nilai
		//var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&nilai.ID,
			&nilai.Indeks,
			&nilai.Skor,
			&nilai.Mahasiswa_ID,
			&nilai.Mata_Kuliah_ID); err != nil {
			return nil, err
		}
		dataNilai = append(dataNilai, nilai)
	}
	return dataNilai, nil
}

// GetId
func GetIdMhs(ctx context.Context, idMhs string) ([]models.Nilai, error) {
	var dataNilai []models.Nilai
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v WHERE mahasiswa_id=%v;",
		table,
		idMhs,
	)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var nilai models.Nilai
		//var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&nilai.ID,
			&nilai.Indeks,
			&nilai.Skor,
			&nilai.Mahasiswa_ID,
			&nilai.Mata_Kuliah_ID); err != nil {
			return nil, err
		}
		dataNilai = append(dataNilai, nilai)
	}
	return dataNilai, nil
}

// Insert
func Insert(ctx context.Context, dataNilai models.Nilai, idMhs string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	dataNilai.Indeks = utils.GetIndeks(uint(dataNilai.Skor))
	dataNilai.ID = uint(time.Now().Unix())
	queryText := fmt.Sprintf("INSERT INTO %v values(%v, '%v', %v, %v, %v)",
		table,
		dataNilai.ID,
		dataNilai.Indeks,
		dataNilai.Skor,
		idMhs,
		dataNilai.Mata_Kuliah_ID)
	_, err = db.ExecContext(ctx, queryText)

	if dataNilai.Skor > 100 {
		return errors.New("nilai tidak boleh lebih dari 100")
	}

	if err != nil {
		return err
	}
	return nil
}

// Update
func Update(ctx context.Context, dataNilai models.Nilai, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	if dataNilai.Skor > 100 {
		return errors.New("nilai tidak boleh lebih dari 100")
	} else {
		dataNilai.Indeks = utils.GetIndeks(dataNilai.Skor)
		queryText := fmt.Sprintf("UPDATE %v set skor ='%v', indeks = '%v' where id = %v",
			table,
			dataNilai.Skor,
			dataNilai.Indeks,
			id,
		)

		_, err = db.ExecContext(ctx, queryText)
	}

	if err != nil {
		return err
	}

	return nil
}

// Delete
func Delete(ctx context.Context, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = %v", table, id)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ditemukan")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}
