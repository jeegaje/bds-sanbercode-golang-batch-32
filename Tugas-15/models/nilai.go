package models

type Nilai struct {
	ID             uint   `json:"id"`
	Indeks         string `json:"indeks"`
	Skor           uint   `json:"skor"`
	Mahasiswa_ID   uint   `json:"mahasiswa_id"`
	Mata_Kuliah_ID uint   `json:"mata_kuliah_id"`
}
