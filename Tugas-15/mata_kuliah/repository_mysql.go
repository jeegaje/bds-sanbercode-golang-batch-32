package mata_kuliah

import (
	"Tugas-15/config"
	"Tugas-15/models"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
)

const (
	table = "mata_kuliah"
)

// GetAll
func GetAll(ctx context.Context) ([]models.Mata_Kuliah, error) {
	var dataMk []models.Mata_Kuliah
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v", table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var mata_kuliah models.Mata_Kuliah
		//var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&mata_kuliah.ID,
			&mata_kuliah.Nama); err != nil {
			return nil, err
		}
		dataMk = append(dataMk, mata_kuliah)
	}
	return dataMk, nil
}

// Insert
func Insert(ctx context.Context, dataMk models.Mata_Kuliah) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	dataMk.ID = uint(time.Now().Unix())
	queryText := fmt.Sprintf("INSERT INTO %v values(%v, '%v')",
		table,
		dataMk.ID,
		dataMk.Nama)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

// Update
func Update(ctx context.Context, dataMk models.Mata_Kuliah, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	queryText := fmt.Sprintf("UPDATE %v set nama ='%v' where id = %v",
		table,
		dataMk.Nama,
		id,
	)

	_, err = db.ExecContext(ctx, queryText)
	if err != nil {
		return err
	}

	return nil
}

// Delete
func Delete(ctx context.Context, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = %v", table, id)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ditemukan")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}
