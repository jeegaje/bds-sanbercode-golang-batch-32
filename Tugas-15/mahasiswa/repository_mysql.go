package mahasiswa

import (
	"Tugas-15/config"
	"Tugas-15/models"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
)

const (
	table = "mahasiswa"
)

// GetAll
func GetAll(ctx context.Context) ([]models.Mahasiswa, error) {
	var dataMhs []models.Mahasiswa
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v", table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var mahasiswa models.Mahasiswa
		//var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&mahasiswa.ID,
			&mahasiswa.Nama); err != nil {
			return nil, err
		}
		dataMhs = append(dataMhs, mahasiswa)
	}
	return dataMhs, nil
}

// Insert
func Insert(ctx context.Context, dataMhs models.Mahasiswa) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	dataMhs.ID = uint(time.Now().Unix())
	queryText := fmt.Sprintf("INSERT INTO %v values(%v, '%v')",
		table,
		dataMhs.ID,
		dataMhs.Nama)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

// Update
func Update(ctx context.Context, dataMhs models.Mahasiswa, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	queryText := fmt.Sprintf("UPDATE %v set nama ='%v' where id = %v",
		table,
		dataMhs.Nama,
		id,
	)

	_, err = db.ExecContext(ctx, queryText)
	if err != nil {
		return err
	}

	return nil
}

// Delete
func Delete(ctx context.Context, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = %v", table, id)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ditemukan")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}
