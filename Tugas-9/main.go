package main

import (
	"Tugas-9/library"
	"fmt"
	"strconv"
)

func main() {
	// Soal 1
	fmt.Println("----Soal 1----")
	var bangunDatar library.HitungBangunDatar
	var bangunRuang library.HitungBangunRuang
	bangunDatar = library.SegitigaSamaSisi{Alas: 8, Tinggi: 3}
	fmt.Println("===== Segitiga sama sisi")
	fmt.Println("luas      :", bangunDatar.Luas())
	fmt.Println("keliling  :", bangunDatar.Keliling())
	bangunDatar = library.PersegiPanjang{Panjang: 4, Lebar: 8}
	fmt.Println("===== Persegi Panjang")
	fmt.Println("luas      :", bangunDatar.Luas())
	fmt.Println("keliling  :", bangunDatar.Keliling())
	bangunRuang = library.Tabung{JariJari: 7, Tinggi: 5}
	fmt.Println("===== Tabung")
	fmt.Println("volume      :", bangunRuang.Volume())
	fmt.Println("luas permukaan  :", bangunRuang.LuasPermukaan())
	bangunRuang = library.Balok{Panjang: 4, Lebar: 6, Tinggi: 8}
	fmt.Println("===== Balok")
	fmt.Println("volume      :", bangunRuang.Volume())
	fmt.Println("luas permukaan  :", bangunRuang.LuasPermukaan())

	// Soal 2
	fmt.Println("----Soal 2----")
	var hape library.Gadget
	hape = library.Phone{
		Name:   "Galaxy Note 20",
		Brand:  "Samsung",
		Year:   2020,
		Colors: []string{"Mysthic Brown", "Mysthic White", "Mysthic Black"},
	}
	fmt.Println(hape.ShowData())

	// Soal 3
	fmt.Println("----Soal 3----")
	fmt.Println(library.LuasPersegi(4, true))

	fmt.Println(library.LuasPersegi(8, false))

	fmt.Println(library.LuasPersegi(0, true))

	fmt.Println(library.LuasPersegi(0, false))

	// Soal 4
	fmt.Println("----Soal 4----")
	var prefix interface{} = "hasil penjumlahan dari "

	var kumpulanAngkaPertama interface{} = []int{6, 8}

	var kumpulanAngkaKedua interface{} = []int{12, 14}

	// tulis jawaban anda disini

	var jumlah int

	kumpulanAngka := kumpulanAngkaPertama.([]int)

	kalimat := prefix.(string)

	angkaKedua := kumpulanAngkaKedua.([]int)
	kumpulanAngka = append(kumpulanAngka, angkaKedua...)

	for index, item := range kumpulanAngka {
		jumlah += item
		if index == len(kumpulanAngka)-1 {
			kalimat += strconv.Itoa(item) + "+" + strconv.Itoa(jumlah)

		} else {
			kalimat += strconv.Itoa(item) + "+"
		}
	}

	fmt.Println(kalimat)
}
