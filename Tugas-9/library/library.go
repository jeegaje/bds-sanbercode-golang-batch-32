package library

import (
	"math"
	"strconv"
)

type SegitigaSamaSisi struct {
	Alas, Tinggi int
}

type PersegiPanjang struct {
	Panjang, Lebar int
}

type Tabung struct {
	JariJari, Tinggi float64
}

type Balok struct {
	Panjang, Lebar, Tinggi int
}

type HitungBangunDatar interface {
	Luas() int
	Keliling() int
}

type HitungBangunRuang interface {
	Volume() float64
	LuasPermukaan() float64
}

func (s SegitigaSamaSisi) Luas() int {
	return s.Alas * s.Tinggi / 2
}

func (s SegitigaSamaSisi) Keliling() int {
	alas := s.Alas / 2
	sisi := int(math.Sqrt(float64(alas*alas + s.Tinggi*s.Tinggi)))
	return sisi + sisi + sisi
}

func (p PersegiPanjang) Luas() int {
	return p.Lebar * p.Panjang
}

func (p PersegiPanjang) Keliling() int {
	return (p.Lebar + p.Panjang) * 2
}

func (t Tabung) Volume() float64 {
	return math.Pi * t.JariJari * t.JariJari * t.Tinggi
}

func (t Tabung) LuasPermukaan() float64 {
	alas := 2 * math.Pi * t.JariJari * t.JariJari
	selimut := 2 * math.Pi * t.JariJari
	return alas + selimut
}

func (b Balok) Volume() float64 {
	return float64(b.Lebar * b.Panjang * b.Tinggi)
}

func (b Balok) LuasPermukaan() float64 {
	return float64(2*b.Panjang*b.Lebar + 2*b.Lebar*b.Tinggi + 2*b.Panjang*b.Tinggi)
}

type Phone struct {
	Name, Brand string
	Year        int
	Colors      []string
}

type Gadget interface {
	ShowData() string
}

func (p *Phone) TambahColor(warna ...string) {
	for _, item := range warna {
		p.Colors = append(p.Colors, item)
	}
}

func (p Phone) ShowData() string {
	str := strconv.Itoa(p.Year)
	kalimat := "name : " + p.Name + "\nbrand : " + p.Brand + "\nyear : " + str + "\ncolors : "
	for _, item := range p.Colors {
		kalimat += item + ", "
	}
	return kalimat
}

func LuasPersegi(sisi uint, keterangan bool) interface{} {
	switch {
	case sisi > 0 && keterangan:
		return "luas persegi dengan sisi " + strconv.Itoa(int(sisi)) + " cm adalah " + strconv.Itoa(int(sisi*sisi)) + " cm"
	case sisi > 0 && !keterangan:
		return sisi * sisi
	case sisi == 0 && keterangan:
		return "maaf anda belum menginput sisi persegi!"
	default:
		return nil
	}
}
